/*!
 * MSUBootstrap Gruntfile
 * Copyright 2014 Michigan State University.
 */

module.exports = function (grunt) {
  'use strict';

  // Force use of Unix newlines
  grunt.util.linefeed = '\n';

  // Project configuration.
  grunt.initConfig({

    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*!\n' +
            ' * MSUBootstrap v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
            ' * Copyright 2014-<%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            ' */\n',

    // Task configuration.
    clean: {
      dist: ['dist']
    },

    less: {
      compileCore: {
        options: {
          strictMath: true,
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: '<%= pkg.name %>.css.map',
          sourceMapFilename: 'dist/css/<%= pkg.name %>.css.map'
        },
        files: {
          'dist/css/<%= pkg.name %>.css': '<%= pkg.name %>.less'
        }
      },
      minify: {
        options: {
          cleancss: true,
          report: 'min'
        },
        files: {
          'dist/css/<%= pkg.name %>.min.css': 'dist/css/<%= pkg.name %>.css'
        }
      }
    },

    usebanner: {
      options: {
        position: 'top',
        banner: '<%= banner %>'
      },
      files: {
        src: 'dist/css/*.css'
      }
    },

    copy: {
      fonts: {
        expand: true,
        src: 'bootstrap/dist/fonts/*',
        dest: 'dist/fonts',
        flatten: true
      },
      js: {
        expand: true,
        src: 'bootstrap/dist/js/*',
        dest: 'dist/js',
        flatten: true
      },
      img: {
        expand: true,
        src: 'img/*',
        dest: 'dist',
      },
      html: {
        src: 'template.html',
        dest: 'dist/template.html'
      }
    },

    watch: {
      source: {
        files: ['*.html','*.less'],
        tasks: ['less', 'copy']
      }
    }
  });

  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
  require('time-grunt')(grunt);

  // CSS distribution task.
  grunt.registerTask('less-compile', ['less:compileCore']);
  grunt.registerTask('dist-css', ['less-compile', 'usebanner', 'less:minify']);

  // Full distribution task.
  grunt.registerTask('dist', ['clean', 'dist-css', 'copy:fonts', 'copy:js', 'copy:img', 'copy:html']);

  // Default task.
  grunt.registerTask('default', ['dist']);
};